
public class Converter {
	//Your names go here:
	/*
	* @Author: Name of the first student Nicholas
	* Name of the second student Negar
	* Name of the third student Janna
	*
	*/
	private static double celsiusToFahrenheit(double C){
	double far =(C*(9/5))+32;
	return far;
	}
	private static double fahrenheitToCelsius(double F){
	// TODO: The second student will implement this method
		double Cel = (F-32)*5/9;
		return Cel;
	}
	public static void main(String[] args) {
	//TODO: The first student will implement this method.
	// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
	// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
            System.out.println(celsiusToFahrenheit(180));
            System.out.println(fahrenheitToCelsius(250));
            
            
	}

}
